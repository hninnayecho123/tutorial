<?php
App::uses('AppController', 'controller');
class UsersController extends AppController {

    public function beforeFilter() {
      parent::beforeFilter();
    }

    public function register() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('Successfully Registeration!'));
                $this->redirect(array('action' => 'login'));
            }
             $this->Flash->error(__('Unsuccessfully Registeration!'));
        }
    }
    public function login() { 
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->redirect(array('controller'=>'Topics', 'action'=>'index'));
            }
            $this->Flash->error(__('Incorrect username or password'));
        }
    }

    public function logout() {
        $this->Session->destroy();
        $this->redirect($this->Auth->logout());
    }
} 