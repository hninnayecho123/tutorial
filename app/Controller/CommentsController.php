<?php
class CommentsController extends AppController {   
    public $helpers = array('Html', 'Form');
    var $uses = array('Topic', 'Comment');

    public function add() {
        if ($this->request->is('post')) {
            if(!empty($this->data)) {
                if(!empty($this->data['Comment']['image_field_name']['name'])) {
                    $file = $this->data['Comment']['image_field_name']; //put the  data into a var for easy use
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                    if(in_array($ext, $arr_ext)) {
                        if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload_folder' . DS . $file['name'])) {
                            $this->request->data['Comment']['image_field_name'] = '/img/upload_folder/'.$file['name'];                
                        } else {
                            $this->Session->setFlash(__('The data could not be saved. Please, try again.'), 'default',array('class'=>'errors'));
                            }
                    }
                } else {
                    $this->request->data['Comment']['image_field_name'] = '';
                    }
                $this->request->data['Comment']['user_id'] = $this->Auth->user('id');
                $this->request->data['Comment']['username'] = $this->Auth->user('username');
                if(!empty($this->data['Comment']['image_field_name']['name']) || !empty($this->data['Comment']['comment'])) {
                    if ($this->Comment->save($this->request->data)) {
                    $this->Flash->success(__('Save comment'));
                    $this->redirect(array('controller'=>'topics', 'action'=>'view', $this->data['Comment']['topic_id']));
                    }       
                } else {
                    $this->Flash->error(__('Unable to comment.'));
                    $this->redirect(array('controller'=>'topics', 'action'=>'view', $this->data['Comment']['topic_id']));
                    }  
            }
        }
    }


    /*public function edit($id = null, $topic_id = null) {
        
        $comment = $this->Comment->findById($id);

        if ($this->request->is(array('post', 'put'))) {
            $this->Comment->id = $id;
            $this->Topic->id = $topic_id;
            if ($this->Comment->save($this->request->data)) {
                $this->Flash->success(__('Your comment has been updated.'));
                $this->redirect(array('controller'=>'topics','action' => 'view', $topic_id));
            }
            
            $this->Flash->error(__('Unable to update your comment.'));
        }

        if (!$this->request->data) {
            $this->request->data = $comment;
        }
    }*/

    public function edit($id = null, $topic_id = null) {

        $comment = $this->Comment->findById($id);
        $this->Comment->id = $id;
        $this->Topic->id = $topic_id; 

        if(!empty($this->data)) {
                if(!empty($this->data['Comment']['image_field_name']['name'])) {
                    $file = $this->data['Comment']['image_field_name']; //put the  data into a var for easy use
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                    if(in_array($ext, $arr_ext)) {        
                        if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload_folder' . DS . $file['name'])) {                           
                           $this->request->data['Comment']['image_field_name'] = '/img/upload_folder/'.$file['name'];                
                        } else {
                           $this->Session->setFlash(__('The data could not be saved. Please, try again.'), 'default',array('class'=>'errors'));
                        }
                    }
                } else {
                        $this->request->data['Comment']['image_field_name'] = $this->$comment['Comment']['image_field_name'] ;
                    }

        if ($this->request->is(array('post', 'put'))) {
                 
                if ($this->Comment->save($this->request->data)) {
                    $this->Flash->success(__('Your comment has been updated.'));
                    $this->redirect(array('controller'=>'topics','action' => 'view', $topic_id));
                }
            }
        }
            if (!$this->request->data) {
                $this->request->data = $comment;
            }
    }

    public function delete($id = null, $topic_id = null) {

        $this->Comment->id = $id;
        $this->Topic->id = $topic_id;
        $comment = $this->Comment->findById($id);

        if ($this->Comment->delete($id)) {
            $this->Flash->success(
            __('The comment with id: %s has been deleted.', h($id))
            );
        } else {
            $this->Flash->error(
             __('The comment could not be deleted.')
            );
            }

        $this->redirect(array('controller'=>'topics','action' => 'view', $topic_id));
    }
}