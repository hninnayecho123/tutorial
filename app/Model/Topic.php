<?php

class Topic extends AppModel {
	
	public $hasMany = 'Comment';
	public $belongsTo = 'User';

	public $validate = array(
		'title' => array(
			'rule' => 'notBlank'
		),
		'body' => array(
			'rule' => 'notBlank'
		)
	);
}
