<h2><?php echo h($topic['Topic']['title']); ?></h2>
<p><?php echo h($topic['Topic']['body']); ?></p>
<p><?php echo !empty($topic['Topic']['image_field_name']) ? $this->Html->image($topic['Topic']['image_field_name'], array('width' => '100px','heigh' => '100px')) : ''; ?></p>
<p>Created Date-   <?php echo h($topic['Topic']['created']); ?></p>
<p>Modified Date-   <?php echo h($topic['Topic']['modified']); ?></p>

<h3>Comments</h3>
<table>
	<?php foreach ($topic['Comment'] as $comment): ?>
	<tr>
		<td>
			<td>
				Comment=>
            	<?php echo $comment['comment']; ?>
			</td>
			<td>
            <?php echo !empty($comment['image_field_name']) ? $this->Html->image($comment['image_field_name'], array('width' => '100px','heigh' => '100px')) : ''; ?>
            </td>
        </td>
		<td>
		<?php 
			if ($comment['user_id'] == $this->Session->read('Auth.admins.id')) {
		        echo $this->Html->link('Edit', array('controller' => 'comments', 'action'=>'edit', $comment['id'], $comment['topic_id']));
		        echo "\n";
		        echo $this->Form->postLink('Delete', array('controller' => 'comments', 'action'=>'delete', $comment['id'], $comment['topic_id']), array('confirm'=> 'Are you sure?'));
		    }
	    ?>
	    </td>
    </tr>
	<?php endforeach; ?>
</table>


<h3>Add Comment</h3>
<?php
echo $this->Form->create('Comment', array('enctype' => 'multipart/form-data', 'url' => array('controller'=>'comments', 'action' => 'add')));
echo $this->Form->input('comment', array('rows'=>3));
echo $this->Form->input('Comment.topic_id', array('type'=>'hidden', 'value'=>$topic['Topic']['id']));
echo $this->Form->input("image_field_name",array("type"=>"file"));
echo $this->Form->end('comment');
?>

