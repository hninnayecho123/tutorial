 <h2>Latest Topics</h2>

<table>
    <thead>
        <tr>
        <?php echo $this->Html->link('Add Topic', array('controller'=>'topics','action'=>'add')); ?>
        <td>
             <td>
                <td>
                     <?php echo $this->Html->link('Logout', array('controller'=>'users','action'=>'logout')); ?>
                </td>
               
            </td>
        </td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>Topic</th>
        <th>Actions</th>
    </tr>    
    
    <?php foreach ($topics as $topic) : ?>
    <tr>
    	<td>
       		<?php echo $this->Html->link($topic['Topic']['title'], '/topics/view/'.$topic['Topic']['id']); ?> by <?php echo h( $topic['User']['username']); ?>
        </td>
        <td>
        <?php
        if($topic['Topic']['user_id'] == $this->Session->read('Auth.admins.id')) {
            echo $this->Html->link('Edit', array('controller' => 'topics', 'action'=>'edit',$topic['Topic']['id']));
            echo "\n";
            echo $this->Form->postLink('Delete', array('action' => 'delete', $topic['Topic']['id']), array('confirm' => 'Are you sure?')); 
        }	 
        	?>
        </td> 
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php
    $paginator = $this->Paginator;
    echo "<div class='paging'>";      
    if($paginator->hasPrev()){
        echo $paginator->prev("Prev");
    }
    echo $paginator->numbers(array('modulus' => 2));
    if($paginator->hasNext()){
        echo $paginator->next("Next");
    }    
    echo "</div>";
?>



