<h1>Add Topic</h1>
<?php

echo $this->Form->create('Topic', array('enctype' => 'multipart/form-data', 'url' => array('controller'=>'topics', 'action'=>'add')));
echo $this->Form->input('title');
echo $this->Form->input('body', array('rows' => '3'));
echo $this->Form->input("image_field_name",array("type"=>"file"));
echo $this->Form->end('Save Topic');
?>