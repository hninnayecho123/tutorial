
<h1>Edit Comment</h1>
<?php
echo $this->Form->create('Comment', array('enctype' => 'multipart/form-data', 'url' =>  array('controller' => 'comments', 'action'=>'edit', $this->data['Comment']['id'], $this->data['Comment']['topic_id'])));
echo $this->Form->input('comment', array('rows' => '3'));
echo $this->Form->input('id', array('type' => 'hidden'));
?>
<?php
if(!empty($this->data['Comment']['image_field_name'])): ?>
    <div class="input">
     <label>Current Image:</label>
     <?php
       echo $this->Html->image($this->data['Comment']['image_field_name'], array('width'=>100));
     ?>
    </div>
  <?php endif; ?>
<?php echo $this->Form->input("image_field_name",array("type"=>"file"));
echo $this->Form->end('Save Comment');
?>
<h4><?php echo $this->Html->link( "Return back",   array('controller' => 'topics', 'action'=>'view', $this->data['Comment']['topic_id'])); ?></h4>